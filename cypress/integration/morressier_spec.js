// <reference types="Cypress" />
import CommonFuns from '../../support/pageObjects/commonFunctions.js'
import Utils from '../../support/utilities.js'
import ArticlesTab from '../../support/pageObjects/articlesTab.js'
import SubmissionTab from '../../support/pageObjects/submissionTab.js'

const commonFunctions=new CommonFuns()
const utilitiesFunction=new Utils()
const articles=new ArticlesTab()
const submission= new SubmissionTab()
describe("End to End",function() {
beforeEach(function () {
    cy.fixture('properties').then(function(data) {
        this.data=data;
        
    })
    
});

it ("Session tab & day filters", function() {
    cy.visit(this.data.url)
    commonFunctions.clickOnAcceptCookies()
    commonFunctions.clickOnShowAll()
    utilitiesFunction.scrollDown()
    commonFunctions.selectDisplayedDateRow().contains(this.data.date).scrollIntoView().click()
    utilitiesFunction.validateSelection()    
})

it("Submission tab, Search & filtering", function(){
    submission.clickOnSubmissionTab()
    utilitiesFunction.enterTextInSearch(this.data.authorname)
    utilitiesFunction.validatingSearchResult(this.data.authorname)
    //submission.enterTextInSearch(this.data.text).type('{enter}')
   submission.clearSearchEntered()

    submission.clickOnAddFilterButton()
    submission.enterTextOnFilter().type(this.data.searchtext)
    submission.clickOnCheckbox()
    submission.scrollToVisibleText() 
    commonFunctions.clickOnCloseButton()
    submission.checkKeyword()
    submission.tapOnArticle()

    cy.wait(2000)
    submission.checkForFrame()
    
    commonFunctions.tapOnBackButton()
    submission.clickOnAddFilterButton()
    submission.bringClearToView()
    submission.clearFilters()
    commonFunctions.clickOnCloseButton()

})

it("Article View & Video Playback", function(){
    submission.tapOnArticle()
    articles. tapOnPlayVideo()
    cy.wait(5000)
    articles.clickOnPause
    articles.clickOnPlay
    //cy.scrollTo('top',{ duration: 1000 })
    cy.wait(5000)
    articles.clickOnPause
    utilitiesFunction.scrollDown()
    utilitiesFunction.scrollTop()
    commonFunctions.tapOnBackButton()
    submission.clickOnNextButton()
    submission.clickOnPrevButton() 

    articles.clickOnSpeakers()
    utilitiesFunction.getSpeakers(this.data.authorname)
   commonFunctions.clickonSessionTab()

   

    

})


})


