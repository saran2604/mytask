class articlesTab{

    tapOnPlayVideo(){
        return cy.get('[class="vjs-big-play-button"]').should('have.text','Play Video').click()
     }
     clickOnPause(){
      return  cy.get('[class="vjs-play-control vjs-control vjs-button vjs-playing"]').should('have.text','Pause').click()
     }
     clickOnPlay(){
      return  cy.get('[class="vjs-play-control vjs-control vjs-button vjs-paused"]').scrollIntoView().click()
     }
     clickOnSpeakers(){
         return cy.get('#speakers').click()
     }
//n
}
export default articlesTab
