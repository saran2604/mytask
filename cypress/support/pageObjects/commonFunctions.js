class commonFunctions{


    clickonSessionTab(){
        return cy.get('#sessions').click()
    }
clickOnAcceptCookies(){
    return cy.get('.style__Wrapper-sc-1ogxbzz-0 > .sc-jcwofb').should('have.text','Got it').click()
}
clickOnShowAll(){
    return cy.get('[class="sc-jcwofb dgVqSa SessionContainer__CustomButton-sc-1bqy2jc-4 gcWecE').click()
}
selectDisplayedDateRow(){
    return cy.get('.SessionContainer__DayFiltersWrapper-sc-1bqy2jc-1>button').should('have.length',8)
}
tapOnBackButton(){
    return cy.get('[class="styles__BackToPageLinkWrapper-kg3h9e-12 fmTNXQ"]').should('have.text','Back').click()
 }
 clickOnCloseButton(){
    return cy.get('[class="sc-jSFkmK bGhKbJ"]').should('have.text','Close').click() 
}
//Articles view

}
export default commonFunctions
