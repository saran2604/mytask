class submissionTab{


    clickOnSubmissionTab(){
        return cy.get('#submissions').click()
     }
     enterTextInSearch(){
         return cy.get('[class="sc-hBMVcZ csRXKg"]')
     }
     clearSearchEntered(){
       return cy.get('[style="display: flex;"] > .bpzEwc').click()
     }
     clickOnAddFilterButton(){
         return cy.get('.style__SearchAndFiltersContainer-sc-1fffutc-10 > .sc-jSFkmK').click()
     }
     enterTextOnFilter(){
         return cy.get('[name="filter-Keywords"]')
     }
     clickOnCheckbox(){
        return cy.get('.style__Ddd-sc-1x1tqvy-0 > span').click()
     }
     scrollToVisibleText(){
         return cy.get('#keywordsbh-content > .MuiExpansionPanelSummary-content > .bpzEwc').scrollIntoView()
     }
     
     checkKeyword(){
        return cy.get ('[class="sc-bdnylx cASOld"]').should("be.visible")
     }
     tapOnArticle(){
         return cy.get(':nth-child(2) > .sc-lbVuaH > .gmTauj > div > .sc-jffIyK').click()
     }

     checkForFrame(){
         return cy.get('.ArticleContents__ButtonsContainer-sc-9yuma7-0').should('be.visible').contains('Video')
     }
     bringClearToView(){
         return cy.get('[style="padding: 1rem; margin-bottom: 1rem;"] > .bpzEwc').scrollIntoView()
     }
     
     clearFilters(){
         return cy.get('[class="sc-bdnylx bpzEwc sc-fbIXFq geecTj"]').click() 
     }

     clickOnNextButton(){
         return cy.get('[class="sc-jSFkmK iLQENc"]').scrollIntoView().click()
     }
     clickOnPrevButton(){
         return cy.get('[class="sc-jSFkmK cAZLuU"]').scrollIntoView().click()
     }

     filterSearchCount(){
       return cy.get('.jss646 > .MuiCollapse-container > .MuiCollapse-wrapper > .MuiCollapse-wrapperInner > [role="region"] > .MuiExpansionPanelDetails-root > [style="padding: 4px; display: flex; justify-content: space-between; align-items: flex-start;"] > .sc-bdnylx').text()
    
     }

     countUnderFilter(){
      return  cy.get('[style="display: flex;"] > .sc-bdnylx').text()
     }

}
export default submissionTab 
